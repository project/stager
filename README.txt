
CONTENTS OF THIS FILE
---------------------

 * Summary
 * Installation
 * Cron setup
 * Drush
 * Maintainers


SUMMARY
-------

The stager module makes it possible to create a staging version of
your website where you can make any changes without affecting the 
live site. When you've done your changes in staging mode, you can sync all 
those changes back to the live site.

For a full description and downloads of the module, visit the project page:
  http://drupal.org/project/stager
  
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/stager


INSTALLATION
------------

1. Download the module and place it in sites/all/modules. If you place it
   somewhere else, remember that path, you'll need it later on.

2. Open settings.php and look for the $databases array and 
   add following line before that variable:

   require_once(DRUPAL_ROOT . '/sites/all/modules/stager/stager.module');

   If the module is in another directory, change the path to the module.

3. Still in settings php, look for the 'prefix' key in your database and
   change that to the following:

   'prefix' => (function_exists('stager_pre_bootstrap')) ? stager_pre_bootstrap() : '',

   If you are already using a prefix, put that prefix between the single quotes.

4. Go to admin/modules and enable the module, listed in the 'Other' fieldset.
   Give permissions to users at 'admin/people/permissions'.

5. Go to 'admin/config/system/stager' and set your site into staging mode.
   If all went well, a grey bar should appear at the bottom of your screen with
   following text:

   "You are working on the staging version of your site. All changes are not 
   visible on the live version. Click here to put your changes live."

   You can now make any change without affecting the live version of your site.
   When you're ready, click on the link and sync all changes back and/or remove 
   the staging version of your website.


CRON SETUP
----------

todo


DRUSH
-----

todo


MAINTAINERS
-----------

swentel - http://drupal.org/user/107403
