<?php

/**
 * @file
 * Administration functions for the stager module.
 */

/**
 * The page that shows the state of stager presenting the right forms.
 */
function stager_state() {

  // Create staging site form.
  if (!variable_get('stager_created')) {
    $build = drupal_get_form('stager_create_staging');
  }
  else {
    // The site has a staging version. Determine which form we need
    // to present to the current user. This can be one or a combination of
    // the following forms:
    // 1) Leave staging mode form: The use has access to jump back to
    //    the live version of the site so he can do other actions.
    // 2) Enter staging mode form: The user has access to jump back to
    //    the staging version of the site to make further changes.
    // 3) Sync changes form: The user has access to sync the changes from
    //    the staging version to the live version.
    // 4) Delete staging version: The user has access to remove the
    //    the complete staging version without syncing.
    if (TRUE) {
      if (isset($_COOKIE['stager'])) {
        $build = drupal_get_form('stager_leave_staging_form');
      }
      else {

        $staging_created_time = variable_get('stager_time');
        $user_created = variable_get('stager_user');

        $build['staging_info'] = array(
          '#markup' => '<p>' . t('The staging site has been last synchronized at !date by !user', array('!date' => $staging_created_time, '!user' => $user_created)) . '</p>'
        );

        $build['stager_enter_form'] = drupal_get_form('stager_enter_staging_form');
        $build['stager_sync_form'] = drupal_get_form('stager_sync_changes_form');
        $build['stager_remove_form'] = drupal_get_form('stager_remove_staging_form');
      }
    }
  }

  return $build;
}

/**
 * Create staging form.
 */
function stager_create_staging($form) {

  $form['info'] = array(
    '#markup' => '<p>' . t('Click on the button underneath to create a staging version of your site and start making changes to it. This operation might take a while.') . '</p>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create staging site'),
  );

  return $form;
}

/**
 * Create staging form submit callback.
 */
function stager_create_staging_submit($form, &$form_state) {

  // Save stager variables.
  stager_staging_info_create();

  // Log action.
  stager_log('Staging site has been created.');

  // Enter staging mode.
  stager_enter_staging_mode();

  // Create staging site.
  stager_staging_create();

  // Redirect so cookie takes effect and the right message is shown.
  $form_state['redirect'] = 'admin/config/system/stager/action/create_staging';
}

/**
 * Enter staging site.
 */
function stager_enter_staging_form() {

  $form['info'] = array(
    '#markup' => '<p>' . t('Click on the button underneath to enter staging mode to start making new changes.') . '</p>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Enter staging mode'),
  );

  return $form;
}

/**
 * Enter the staging site.
 */
function stager_enter_staging_form_submit($form, &$form_state) {
  stager_enter_staging_mode();
  $form_state['redirect'] = 'admin/config/system/stager/action/enter_staging';
}

/**
 * Stager leave staging form.
 */
function stager_leave_staging_form() {

  $form['info'] = array(
    '#markup' => '<p>' . t('Click on the button underneath to leave staging mode. Changes will not yet be synchronized to the live site.') . '</p>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Leave staging mode'),
  );

  return $form;
}

/**
 * Stager leaver staging mode submit callback.
 */
function stager_leave_staging_form_submit($form, &$form_state) {
  stager_leave_staging_mode();
  $form_state['redirect'] = 'admin/config/system/stager/action/leave_staging';
}

/**
 * Stager sync changes form.
 */
function stager_sync_changes_form() {

  $form['info'] = array(
    '#markup' => '<p>' . t('Click on the button underneath to sync all changes from the staging version to live.') . '</p>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Sync staging to live'),
  );

  return $form;
}

/**
 * Stager sync changes submit callback.
 */
function stager_sync_changes_form_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/config/system/stager/stager_sync_staging_confirm';
}

/**
 * Sync changes confirm form.
 */
function stager_sync_staging_confirm($form) {
  return confirm_form($form, t('Are you sure you want to sync changes from the staging version to the live site ? All data on the live site will be overwritten.'), 'admin/config/system/stager');
}

/**
 * Sync changes confirm submit callback.
 */
function stager_sync_staging_confirm_submit($form, &$form_state) {

  // Perform operation.
  stager_staging_sync_and_or_remove(TRUE, FALSE);

  // Log action.
  stager_log('Staging site has been synchronized.');

  $form_state['redirect'] = 'admin/config/system/stager';
}

/**
 * Stager remove staging form.
 */
function stager_remove_staging_form($form) {

  $form['info'] = array(
    '#markup' => '<p>' . t('Click on the button underneath to remove the staging version of your site.') . '</p>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Remove staging site'),
  );

  return $form;
}

/**
 * Stager remove staging form submit callback.
 */
function stager_remove_staging_form_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/config/system/stager/stager_remove_staging_confirm';
}

/**
 * Remove staging confirm form.
 */
function stager_remove_staging_confirm($form) {
  return confirm_form($form, t('Are you sure you want to remove the staging version of your site ?'), 'admin/config/system/stager');
}

/**
 * Remove staging confirm submit callback.
 */
function stager_remove_staging_confirm_submit($form, &$form_state) {

  // Perform operation.
  stager_staging_sync_and_or_remove(FALSE);

  // Leave staging mode.
  stager_leave_staging_mode(TRUE);

  // Remove lock.
  stager_staging_info_remove();

  // Log action.
  stager_log('Staging site has been removed.');

  $form_state['redirect'] = 'admin/config/system/stager';
}
